#include <stdio.h>
#include <string.h>

typedef struct employee
{
    int id;
    char name[20];
    float sal;
    struct employee * next;
} emp;

void main()
{
    emp obj1 , obj2 , obj3 ;

    emp * head = &obj1;

    obj1.id=1;
    strcpy(obj1.name,"Kanha");
    obj1.sal=50.0;
    obj1.next=&obj2;
    
    obj2.id=2;
    strcpy(obj2.name,"Ashish");
    obj2.sal=60.0;
    obj2.next=&obj3;
    
    obj3.id=3;
    strcpy(obj3.name,"Badhe");
    obj3.sal=70.0;
    obj3.next=NULL;

    printf("Obj1 Data:\n");
    printf("%d\n",head->id);
    printf("%s\n",head->name);
    printf("%f\n\n",head->sal);
    
    printf("Obj2 Data:\n");
    printf("%d\n",head->next->id);
    printf("%s\n",head->next->name);
    printf("%f\n\n",head->next->sal);

    printf("Obj3 Data:\n");
    printf("%d\n",head->next->next->id);
    printf("%s\n",head->next->next->name);
    printf("%f\n",head->next->next->sal);
    
}