#include<stdio.h>
#include<string.h>

struct company {
    int empNo;
    char name[20];
    float rev;
    struct company * next ;
};

void main(){
    struct company c1 , c2 , c3 ; 
    struct company * head = &c1;

    head->empNo=1000;
    strcpy(head->name,"Hexa");
    head->rev=5500000;
    head->next= &c2;

    head->next->empNo=2000;
    strcpy(head->next->name,"Octa");
    head->next->rev=10000000;
    head->next->next = &c3;

    head->next->next->empNo=30000;
    strcpy(head->next->next->name,"Nine");
    head->next->next->rev=42400000;
    head->next->next->next=NULL;


    printf("Company 1 : \n");
    printf("Number of employees : %d\n",head->empNo);
    printf("Company name : %s\n",head->name);
    printf("Company Revenue : %f\n\n",head->rev);

    printf("Company 2 : \n");
    printf("Number of employees : %d\n",head->next->empNo);
    printf("Company name : %s\n",head->next->name);
    printf("Company Revenue : %f\n\n",head->next->rev);
    
    printf("Company 3 : \n");
    printf("Number of employees : %d\n",head->next->next->empNo);
    printf("Company name : %s\n",head->next->next->name);
    printf("Company Revenue : %f\n\n",head->next->next->rev);
    


}