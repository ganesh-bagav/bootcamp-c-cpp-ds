#include<stdio.h>
#include<string.h>

struct batsman{
    int jerno;
    char name[20];
    float avg;
    struct batsman * next ;
};
void main(){
    struct batsman obj3 = { 45 , "Rohit" , 65.00 , NULL };
    struct batsman obj2 = { 7 , "Dhoni" , 55.00 , &obj3 };
    struct batsman obj1 = { 18 , "Virat" , 50.00 , &obj2 };

    struct batsman * head ;
    head = &obj1 ;

    struct batsman * temp = head ; 
    while (temp != NULL )
    {
        printf("%d\n",temp->jerno);
        printf("%s\n",temp->name);
        printf("%f\n",temp->avg);
        temp = temp->next;
    }
    

}


