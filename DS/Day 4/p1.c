// Creating Node Using Function 

#include<stdio.h>
#include<stdlib.h>

typedef struct student {
    int id ;
    float marks;
    struct student * next ;
}std;

std * head = NULL;

void addNode(){
    std * newNode = (std*) malloc(sizeof(std));
    newNode->id=1;
    newNode->marks=100;
    newNode->next=NULL;
    head=newNode;
}

void printLL(){
    std*tmp=head;
    while (tmp != NULL)
    {
        printf("%d  %f \n",tmp->id,tmp->marks);
        tmp=tmp->next;
    }
    
}
void main(){
    addNode();
    printLL();
}