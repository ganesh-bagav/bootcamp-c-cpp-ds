#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct movie{
    char name[20];
    int ticket;
    float ticPrice;
    struct movie  * next; 
}mv;

void setData(mv * ptr){
    printf("Enter Movie Name : ");
    scanf( "%s", &(ptr->name) );
    printf("Enter No. of tickets : ");
    scanf("%d",&ptr->ticket);
    printf("Enter price of tickets : ");
    scanf("%f",&ptr->ticPrice);
}

void printData(mv*ptr){
    printf("%s\n",ptr->name);
    printf("%d\n",ptr->ticket);
    printf("%f\n",ptr->ticPrice);
    printf("\n");
}
void main(){
    mv m1 , m2 , m3 ;
    setData(&m1);
    setData(&m2);
    setData(&m3);
    printData(&m1);
    printData(&m2);
    printData(&m3);

}