
// Creating First Linked List 

#include<stdio.h>
#include<stdlib.h>

typedef struct Node {
    int data ;
    struct Node * next ; 
}node;

void main(){
    
    // HEAD
    node * head = NULL;
    
    // First Node
    node * newNode = (node*) malloc(sizeof(node));

    newNode->data=10;
    newNode->next=NULL;
    
    head=newNode;

    // Second Node
    newNode = (node*) malloc(sizeof(node));
    newNode->data=20;
    newNode->next=NULL;

    head->next=newNode;

    // Third Node
    newNode = (node*) malloc(sizeof(node));
    newNode->data=30;
    newNode->next=NULL;

    head->next->next=newNode;

    node*temp=head;
    while (temp != NULL )
    {   
        if(temp->next!=NULL){
            printf(" | %d | ->",temp->data);
        }else{
            printf(" | %d | ",temp->data);
        }
        temp=temp->next;

    }
    
    
}
