#include <stdio.h>
#include <stdlib.h>

typedef struct Demo
{
    int data;
    struct Demo *next;
} dm ;

dm *head = NULL;

dm *createNode()
{

    dm *newNode = (dm *)malloc(sizeof(dm));


    printf("Enter Data : ");
    scanf("%d", &newNode->data);

    getchar();

    newNode->next = NULL;

    return newNode;
}

dm *createdmtyNode()
{

    dm *newNode = (dm *)malloc(sizeof(dm));
    newNode->data = 0;
    newNode->next = NULL;

    return newNode;
}

void addNode()
{
    dm *newNode = createNode();
    if (head == NULL)
    {
        head = newNode;
    }
    else
    {
        dm *tmp = head;
        while (tmp->next != NULL)
        {
            tmp = tmp->next;
        }
        tmp->next = newNode;
    }
}

void addAtPos()
{
    int pos = 0;
    while (pos <= 0)
    {
        printf("Enter Position : ");
        scanf("%d", &pos);
        getchar();
        if (pos <= 0)
        {
            printf("Please Enter Valid position \n");
            pos = 0;
        }
    }

    dm *tmp = head;
    if (tmp == NULL && pos == 1)
    {
        dm *newNode = createNode();
        head = newNode;
        tmp = head;
        return;
    }
    else if(pos == 1)
    {
        dm *newNode = createNode();
        newNode->next=head;
        head=newNode;
        return;
    }
    while (pos - 2 > 0)
    {
        if (tmp == NULL)
        {
            dm *newNode = createdmtyNode();
            head = newNode;
            tmp = head;
        }
        else if (tmp->next == NULL)
        {
            dm *newNode = createdmtyNode();
            tmp->next = newNode;
            tmp = tmp->next;
        }
        else
        {
            tmp = tmp->next;
        }
        pos--;
    }

    dm *newNode = createNode();
    if (tmp->next == NULL)
    {
        tmp->next = newNode;
    }
    else
    {
        newNode->next = tmp->next;
        tmp->next = newNode;
    }
}

void addFirst()
{
    dm *newNode = createNode();
    newNode->next = head;
    head = newNode;
}

void deleteFirst(){
    dm * tmp = head;
    head=head->next;
    free(tmp);
}

void deleteLast(){
    dm * tmp = head;
    if(head==NULL){
        return;
    }else if(head->next==NULL){
        free(head);
        head=NULL;
        return;
    }
    while (tmp->next->next != NULL)
    {
        tmp=tmp->next;   
    }
    free(tmp->next);
    tmp->next=NULL;
}

void printLL()
{
    dm *tmp = head;
    while (tmp != NULL)
    {
        if (tmp->next != NULL)
        {
            printf("|%d|->", tmp->data);
        }
        else
        {
            printf("|%d|", tmp->data);
        }
        tmp = tmp->next;
    }
    printf("\n");
}

void countLL()
{
    dm *tmp = head;
    int cnt = 0;
    while (tmp != NULL)
    {
        cnt++;
        tmp = tmp->next;
    }
    printf("\n count : %d \n", cnt);
}

void main()
{
    addNode();
    printLL();

    addNode();
    printLL();

    addNode();
    printLL();

    addFirst();
    printLL();

    addAtPos();
    printLL();

    addAtPos();
    printLL();

    deleteFirst();
    printLL();

    addAtPos();
    printLL();

    addAtPos();
    printLL();

    deleteLast();
    printLL();


    countLL();
}