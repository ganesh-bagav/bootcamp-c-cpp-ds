#include <stdio.h>
#include <stdlib.h>

typedef struct employee
{
    char name[20];
    int id;
    struct employee *next;
} emp;

emp *head = NULL;

emp *createNode()
{

    emp *newNode = (emp *)malloc(sizeof(emp));
    printf("Enter Name : ");
    int i = 0;
    char ch = ' ';
    while ((ch = getchar()) != '\n')
    {
        newNode->name[i] = ch;
        i++;
    }

    printf("Enter ID : ");
    scanf("%d", &newNode->id);

    getchar();

    newNode->next = NULL;

    return newNode;
}

emp *createEmptyNode()
{

    emp *newNode = (emp *)malloc(sizeof(emp));
    newNode->id = 0;
    newNode->name[0] = '-';
    newNode->name[0] = '\0';
    newNode->next = NULL;

    return newNode;
}

void addNode()
{
    emp *newNode = createNode();
    if (head == NULL)
    {
        head = newNode;
    }
    else
    {
        emp *tmp = head;
        while (tmp->next != NULL)
        {
            tmp = tmp->next;
        }
        tmp->next = newNode;
    }
}

void addAtPos()
{
    int pos = 0;
    while (pos <= 0)
    {
        printf("Enter Position : ");
        scanf("%d", &pos);
        getchar();
        if (pos <= 0)
        {
            printf("Please Enter Valid position \n");
            pos = 0;
        }
    }

    emp *tmp = head;
    if (tmp == NULL && pos == 1)
    {
        emp *newNode = createNode();
        head = newNode;
        tmp = head;
        return;
    }
    else if(pos == 1)
    {
        emp *newNode = createNode();
        newNode->next=head;
        head=newNode;
        return;
    }
    while (pos - 2 > 0)
    {
        if (tmp == NULL)
        {
            emp *newNode = createEmptyNode();
            head = newNode;
            tmp = head;
        }
        else if (tmp->next == NULL)
        {
            emp *newNode = createEmptyNode();
            tmp->next = newNode;
            tmp = tmp->next;
        }
        else
        {
            tmp = tmp->next;
        }
        pos--;
    }

    emp *newNode = createNode();
    if (tmp->next == NULL)
    {
        tmp->next = newNode;
    }
    else
    {
        newNode->next = tmp->next;
        tmp->next = newNode;
    }
}

void addFirst()
{
    emp *newNode = createNode();
    newNode->next = head;
    head = newNode;
}

void printLL()
{
    emp *tmp = head;
    while (tmp != NULL)
    {
        if (tmp->next != NULL)
        {
            printf("|%s|%d|->", tmp->name, tmp->id);
        }
        else
        {
            printf("|%s|%d|", tmp->name, tmp->id);
        }
        tmp = tmp->next;
    }
}

void countLL()
{
    emp *tmp = head;
    int cnt = 0;
    while (tmp != NULL)
    {
        cnt++;
        tmp = tmp->next;
    }
    printf("\n count : %d \n", cnt);
}

void main()
{
    addNode();
    addNode();
    addNode();
    addFirst();
    addAtPos();
    addAtPos();
    addAtPos();
    addAtPos();
    printLL();
    countLL();
}