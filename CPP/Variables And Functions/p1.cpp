#include<iostream>

int main()
{
    int x=10;
    int y(20);
    int z{30};

    int &p=x;
    int &q=y;
    int &r=z;

    std::cout<<x<<std::endl;
    std::cout<<y<<std::endl;
    std::cout<<z<<std::endl;
    std::cout<<p<<std::endl;
    std::cout<<q<<std::endl;
    std::cout<<r<<std::endl;
    std::cout<<sizeof(r)<<std::endl; //4

    return 0;
}
