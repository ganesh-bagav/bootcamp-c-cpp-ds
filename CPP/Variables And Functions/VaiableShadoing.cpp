#include <iostream>

int x = 200;
int main()
{

    int x = 10;
    std::cout << x << std::endl; // 10

    {
        int& outer_x = x;  // auto& outer_x = x;
        int x=20;
        {
            int &outer_outer_x = outer_x;
            int& outer_x = x;
            int x =1000;
            std::cout << x << std::endl; // 1000
            std::cout << "Outer"<<outer_x << std::endl; // 20
            std::cout << "Outer_outer"<<outer_outer_x << std::endl; // 10

        }
        std::cout << "Outer"<<outer_x << std::endl; // 10
        std::cout << x << std::endl; // 20
        std::cout << ::x << std::endl; // 200
        x=30;
        std::cout << x << std::endl;// 30
    }
    std::cout << x << std::endl;// 10

}